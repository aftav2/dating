<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<div id="fb-root"></div>

<?php $this->beginBody() ?>

<div class="wrap">
    <header id="header">
      <div class="container"> 
        <div class="row"> 
         <div class="header-logo pull-left">
          <a class="logo" href="<?php echo Yii::$app->homeUrl;?>"><img src="<?php echo Yii::$app->homeUrl;?>web/images/DummyClientLogo.png"></a>
           </div>
           
        
      <div class="account pull-right">
        <ul class="list">
           <li><a href="#">Have an account?  </a></li>
            <li><a href="<?php echo Yii::$app->homeUrl;?>site/signup" class="sign">Sign up</a></li>
            <?php if (Yii::$app->user->isGuest) { ?>
            <li><a href="<?php echo Yii::$app->homeUrl;?>site/login" class="sign">Sign In</a></li>
            <?php } else{ ?>
             <li><a href="<?php echo Yii::$app->homeUrl;?>site/logout" data-method="post" class="sign">Logout(<?php echo Yii::$app->user->identity->username; ?>)</a></li>
           <?php  } ?>
             </ul></div> 
            </div>
           
    <?php
    /*NavBar::begin([
        'brandLabel' => 'Www.Mutoka.com',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
//    $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
//    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Sign In', 'url' => ['/site/login']];
		//$menuItems[] = ['label' => 'Sign Up', 'url' => ['/site/logi']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();*/
    ?>

    
      
        <?= $content ?>
 </div>

<div class="end">
   <div class="container">
        <div class="row">
        
        <div class="col-lg-6 rr">
        <p class="1">ALL RIGHTS RESERVED 2016</p>
         </div>
         
        <div class="col-lg-6">
          <ul class="nn ">
           <li><a href="#">CONTACT US </a> </li>
           <li><a href="#">DISCLOSURES</a></li>
           <li><a href="#">FORUM RULES</a></li>
            </ul>
           </div>
         
  </div>
  </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
   
    



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
